package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

//A custom writer
type logWriter struct{}

func main() {
	resp, err := http.Get("http://www.google.com")

	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	// fmt.Println(resp)

	// bs := make([]byte, 99999) //make a byte slice with 99999 elements or empty spaces
	// resp.Body.Read(bs)        //Use the Reader interface to read the body
	// fmt.Println(string(bs))

	// io.Copy(os.Stdout, resp.Body) // Another way to print the same body on the console using the io.copy function. Internally uses the Writer interface

	//Using the custom Writer
	lw := logWriter{}
	io.Copy(lw, resp.Body)
}

//This makes the logWriter implement the Writer interface
func (lw logWriter) Write(bs []byte) (int, error) {

	fmt.Println(string(bs))
	fmt.Println("Just wrote bytes : ", len(bs))

	return len(bs), nil
}
