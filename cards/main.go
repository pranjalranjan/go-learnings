package main

import "fmt"

func main() {

	//var card string = "Ace of Spades"
	card := "Ace of Spades"

	card = "Five of Diamonds"

	card = newCard()

	fmt.Println(card)

	cards := newDeck()

	fmt.Println(cards)

	cards.print()

	hand, remainingCards := deal(cards, 5)

	hand.print()
	remainingCards.print()

	fmt.Println(hand.toString())

	hand.saveToFile("my_hand")

	newDeck := newDeckFromFile("my_hand")
	newDeck.print()

	cards.shuffle()
	cards.print()
}

func newCard() string {
	return "Ace of Spades"
}
