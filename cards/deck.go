package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

//Create a new type of 'deck' which is a slice of strings

type deck []string

//function newDeck returns a value of type deck. It doesn't need a receiver.
func newDeck() deck {
	cards := deck{}

	cardSuits := []string{"Spades", "Diamonds", "Hearts", "Clubs"}
	cardValues := []string{"Ace", "Two", "Three", "Four"}

	for _, cardSuit := range cardSuits {
		for _, cardValue := range cardValues {
			cards = append(cards, cardValue+" of "+cardSuit)
		}
	}

	return cards
}

func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

func newDeckFromFile(filename string) deck {
	byteDeck, err := ioutil.ReadFile(filename)

	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1)
	}

	s := strings.Split(string(byteDeck), ",")
	return deck(s)
}

func (d deck) shuffle() {

	//Create a truly random number generator with different seed values on every run
	source := rand.NewSource(time.Now().UnixNano()) //unixNano converts the current time into an int64 value
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)

		d[i], d[newPosition] = d[newPosition], d[i] // swap index i with newPosition
	}
}

//receiver function with two arguments. It'll split the deck d into two decks based upon the handsize
//it'll also return both the deck values.
func deal(d deck, handsize int) (deck, deck) {
	return d[:handsize], d[handsize:]
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",") //Convert deck back to slice of strings and join
}

//This is a receiver function. So any variable of type deck gets access to the print method.
//deck is a reference to the type which we want the method to attach to. d is a variable which
// is a reference to the instance variable to be worked on. "d" is only used by convention.
func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}
