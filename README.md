# go-learnings

Learnings of Golang.

Go Learnings

Go is not an OOP language.
Go has two basic data structures that can handle list of records :

1.	Array : Fixed length list of things.
2.	Slice : An array that can go or shrink

Both arrays and slices must be defined with a data type. Hence, a slice or array must contain data of the same type. Slices are zero indexed. Accessing individual elements of a slice is just like java. To select a part of the slice, the following can be used 
sliceName[startIndexIncluding : upToNotIncluding]
eg : fruits[0:2] select elements index 0 and 1.
Fruits[:2] select elements 0 and 1
Fruits[2:] select elements index 2 till the end
Append function doesn’t modify the original slice, it creates a new slice, appends the value after copying the new value and assigns it to the original variable.
Use range and for loop to iterate over the slice.
More basic data types are integer, float, string, map, etc.
Type deck []string : means deck is a new type which extends all the functionality of a slice of string. 
Go doesn’t use “this” or “self”
Go uses only a single letter or two letter argument to a receiver function like func (d deck).
For variables which are not needed, use underscore (_)
Go can return multiple values from a function.
Ioutil package is used for any IO operations. Type conversion or typecasting is just like Java
Strings package has common function to deal with strings. Join function takes a slice of strings and joins them all together with a separator. Split function is the opposite of Join.
Null is ‘nil’ in go.
By default, the go random generator uses the same exact seed value everytime and hence each time a sequence of numbers generated would always be identical to each other. One can create a truly random generator using time, rand and source packages.
To make a test file, create a new file that specifically ends with _test.go . Command to run go tests is go test.
Os package gives file handling related functionalities like delete, create own etc.
Tow slices cannot be compared directly element by element using =. Using =, slice can only be compared to nil
Struct represent structures in Go. Collection pf properties that are related together. When you declare a new struct without initialising its values, go assigns a zero value to it : which is “” for string, 0 for int and float . false for bool etc. It doesn’t assign ‘nil’ by default. Structs can be embedded within structs
Go is a pass by value language. Thus, whenever a struct is passed into g, it copies it somewhere into memory and then made available to the function. Thus, whenever the function acts on the object, it actually acts on the copy and not on the original object. Hence, typical dereferencing and address logic working with pointers applies to go. However, a slice is passed as is by reference and not by value. 
Behind the scenes, when a slice is created, go creates two data structures:
1.	The underlying array with the actual list of times.
2.	The slice with 3 elements: a pointer to the head of the array, capacity which is the maximum elements the array can contain and length which is the number of elements presently in the array. This is the element which is passed to the function when called. It doesn’t pass the underlying array.
Slices, maps, channels, pointers, functions are all passed by reference. Int, float, string, bool, structs are passed by value. Pass by reference still makes a copy of the overlying data structure but since the underlying data structure is the same, it appears that the operation is done on the same object
Shortcut in go: Even if an object is passed while calling to a receiver function which is expecting a pointer to the object, it’s fine syntactically. Go internally transforms the variable to a pointer and makes it available to the function
If any other type in a go program that implements the functions of an interface , they automatically become a member of that type of interface
In go, multiple interfaces can be taken and grouped together to form a new interface
The read function inside Reader interface doesn’t change the size of the byte slice passed to it. It instead only reads the number of bytes equal to the length of the slice.
Channels and go routines are designed in go to handle concurrency. Go routine can be thought of as an engine that executes the code. To launch a new go routine place the word go behind the function.
Go Scheduler, by default works with only one CPU on the machine. Thus, even if multiple go routines are launched only one of them runs at a time. Scheduler runs one routine until it finishes or makes a blocking call (like an HTTP request). The default setting of using only core can be changed. In this case scheduler will run one routine on each core. In this sense, parallelism is achieved.
The execution of the entire program depends on the time the main go routine is executing. To avoid this, channels are used. Channels are used to communicate between go routines. Information that is passed into a channel to be shared between the different routines must be of the same type. Channels specify the type of message that can be shared around.
Receiving a channel over a message is a blocking call.
In go, a function literal is an unnamed function which is used to wrap a piece of code, so that it can be executed anytime in the future.
