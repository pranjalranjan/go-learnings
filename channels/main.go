package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://facebook.com",
		"http://google.com",
		"http://golang.org",
		"http://amazon.com",
		"http://stackoverflow.com",
	}

	c := make(chan string) // Create a channel for the go routines

	//Serially checking all the links
	for _, link := range links {
		go checkLinkStatus(link, c) //Creates child go routines for checking each link
	}

	// //Create a loop for main to wait for all the routines.
	// for i := 0; i < len(links); i++ {
	// 	fmt.Println(<-c) // Receiving a message from the channel once it is present.
	// }

	// //Continuously loop to check status for all websites.
	// for {
	// 	go checkLinkStatus(<-c, c)
	// }

	// //Wait for the channel to return some value. Once the value is returned , assign it to l.
	// for l := range c {
	// 	go checkLinkStatus(l, c) // Equivalent to the above loop
	// }

	//With a literal function
	for l := range c {
		go func(link string) {
			time.Sleep(3 * time.Second) // Wait for 3 secocnds before executing the status check
			checkLinkStatus(link, c)
		}(l)
	}
}

func checkLinkStatus(link string, c chan string) {
	_, err := http.Get(link)

	if err != nil {
		fmt.Printf("Link %v might be down", link)
		fmt.Println()

		c <- link //Return link here into the channel so that the web status checker can work continuously.
		return
	}

	fmt.Printf("Link %v might be up", link)
	fmt.Println()
	c <- link
}
