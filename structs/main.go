package main

import "fmt"

//A struct to hold contact information of a person
type contactInfo struct {
	email   string
	pincode int
}

type dates struct {
	birthday    string
	anniversary string
}

//A struct to hold types having a first name and a last name
type person struct {
	firstName string
	lastName  string
	contact   contactInfo
	dates     //Another way to embed a struct. Just specify its name which will also be taken as the name of the variable
}

func main() {
	//Various ways to initialize a person
	alex := person{"Alex", "Amderson", contactInfo{"alex.anderson@xyz.com", 110001}, dates{"11/01/1991", "11/01"}}
	chris := person{
		firstName: "Chris",
		lastName:  "Anderson",
		contact: contactInfo{
			email:   "chris.anderson@xyq.com",
			pincode: 110001,
		},
		dates: dates{
			birthday:    "11/01/1991",
			anniversary: "11/01",
		},
	}

	fmt.Println(chris)

	var steven person
	fmt.Println(steven) // prints zero values and not nil

	//Assign values to steven now
	steven.firstName = "Steven"
	steven.lastName = "Anderson"
	steven.contact.email = "steven.anderson@xyz.com"
	steven.contact.pincode = 110001
	steven.dates.birthday = "11/02/1991"
	steven.dates.anniversary = "11/02"

	fmt.Println(alex)
	fmt.Println(steven)

	fmt.Printf("%+v", steven) //One more way to print a struct. Prints a struct with all its fields and their values.

	steven.print()

	steven.updateName("jim")
	steven.print() //steven's name is not updated

	stevenPointer := &steven //Passing pointer to update the value actually
	stevenPointer.updateNameWithPointer("jim")
	steven.print()

	steven.updateNameWithPointer("alex") //This works the same way as above. A shortcut. Go will interally create the pointer and pass accordingly.
	steven.print()
}

//Receiver function which acts on structs
func (p person) print() {
	fmt.Printf("%+v", p)
	fmt.Println()
}

//function to represent that functions are passed by value
func (p person) updateName(newFirstName string) {
	p.firstName = newFirstName
}

func (pointerToPerson *person) updateNameWithPointer(newFirstName string) {
	(*pointerToPerson).firstName = newFirstName
}
