package main

import "fmt"

func main() {
	colors := map[string]string{
		"red":   "#ff0000",
		"green": "#00ff00",
	}

	colorsMap := make(map[string]string) // Another way to create maps
	colorsMap["White"] = "#ffffff"       //Put a key in the map

	fmt.Println(colors)
	fmt.Println(colorsMap)

	delete(colors, "red") // Delete a key in the map
	fmt.Println(colors)

	colors["white"] = "#ffffff"
	colors["red"] = "#ff0000"

	printMap(colors)

}

//Function to iterate over a map
func printMap(c map[string]string) {

	for key, value := range c {
		fmt.Printf("Hex code for %v is %v", key, value)
		fmt.Println()
	}
}
